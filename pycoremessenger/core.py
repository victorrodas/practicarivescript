import os, sys
import requests
import json


class Core:
    token = "hola"
    json = None
    token_page = ""

    def __init__(self, token, token_page):
        self.token = token
        self.token_page = token_page

    def InsertJson(self, json):
        self.json = json

    def IsPage(self) -> bool:
        return self.json["object"] == "page"

    def CoreEntry(self):
        if self.IsPage():
            for entry in self.json["entry"]:
                return entry
        return None

    def WhatEvent(self):
        if self.CoreEntry().get("messaging"):
            return "messaging"
        if self.CoreEntry().get("standby"):
            return "standby"
        return None

    def GetEvent(self):
        if self.CoreEntry().get("messaging"):
            for event in self.CoreEntry()["messaging"]:
                return event
        if self.CoreEntry().get("standby"):
            for event in self.CoreEntry()["standby"]:
                return event
        return None

    def GetSenderId(self):
        if self.GetEvent().get("postback"):
            return self.GetEvent()["sender"]["id"]
        if self.GetEvent().get("message"):
            return self.GetEvent()["sender"]["id"]
        return None

    def GetRecipientId(self):
        if self.GetEvent().get("postback"):
            return self.GetEvent()["recipient"]["id"]
        if self.GetEvent().get("message"):
            return self.GetEvent()["recipient"]["id"]
        return None

    def GetResult(self):
        if self.GetEvent().get("postback"):
            return self.GetEvent()["postback"]["title"]
        if self.GetEvent().get("message"):
            message = self.GetEvent()["message"]
            if message.get("text"):
                return self.GetEvent()["message"]["text"]
            elif message.get("attachments"):
                return self.GetEvent()["message"]["attachments"]
        return None

    def TipeResult(self):
        if self.GetEvent().get("postback"):
            return "not message"
        if self.GetEvent().get("message"):
            message = self.GetEvent()["message"]
            if message.get("text"):
                return self.GetEvent()["message"]["text"]
            elif message.get("attachments"):
                return self.GetEvent()["message"]["attachments"]
        return None

    def IsMyMessage(self):
        if self.GetEvent().get("delivery"):
            return True

        if self.GetEvent().get("message"):
            message = self.GetEvent()["message"]
            if message.get("app_id"):
                return True
            return False
        return False

    def GetHeaders(self):
        headers = {
            "Content-Type": "application/json"
        }
        return headers

    def GetParams(self):
        params = {
            "access_token": self.token_page
        }
        return params

    def SendMessage(self, recipient_id, message_text):
        data = json.dumps({
            "recipient": {
                "id": recipient_id
            },
            "message": {
                "text": message_text
            }
        })
        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=self.GetParams(),
                          headers=self.GetHeaders(), data=data)
        if r.status_code != 200:
            return r.status_code
        return r.status_code

    def SendImageUrl(self, recipient_id, url_image):
        data = json.dumps({
            "recipient": {
                "id": recipient_id
            },
            "message": {
                "attachment": {
                    "type": "image",
                    "payload": {
                        "url": url_image,
                        "is_reusable": True
                    }
                }
            }
        })
        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=self.GetParams(),
                          headers=self.GetHeaders(), data=data)
        if r.status_code != 200:
            return r.status_code
        return r.status_code

    def SendAudioUrl(self, recipient_id, url_audio):
        data = json.dumps({
            "recipient": {
                "id": recipient_id
            },
            "message": {
                "attachment": {
                    "type": "audio",
                    "payload": {
                        "url": url_audio
                    }
                }
            }
        })
        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=self.GetParams(),
                          headers=self.GetHeaders(), data=data)
        if r.status_code != 200:
            return r.status_code
        return r.status_code

    def SendFileUrl(self, recipient_id, url_file):
        data = json.dumps({
            "recipient": {
                "id": recipient_id
            },
            "message": {
                "attachment": {
                    "type": "file",
                    "payload": {
                        "url": url_file
                    }
                }
            }
        })
        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=self.GetParams(),
                          headers=self.GetHeaders(), data=data)
        if r.status_code != 200:
            return r.status_code
        return r.status_code

    def SendVideoUrl(self, recipient_id, url_video):
        data = json.dumps({
            "recipient": {
                "id": recipient_id
            },
            "message": {
                "attachment": {
                    "type": "file",
                    "payload": {
                        "url": url_video
                    }
                }
            }
        })
        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=self.GetParams(),
                          headers=self.GetHeaders(), data=data)
        if r.status_code != 200:
            return r.status_code
        return r.status_code

    def SendTemplate(self, recipient_id, template):
        data = json.dumps({
            "recipient": {
                "id": recipient_id
            },
            "message": {
                "attachment": template.to_dict()
            }
        })
        print(data)
        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=self.GetParams(),
                          headers=self.GetHeaders(), data=data)
        if r.status_code != 200:
            return r.status_code
        return r.status_code

    def SendTemplatePrueba(self, recipient_id, template):
        data = json.dumps({
            "recipient": {
                "id": recipient_id
            },
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": [
                            {
                                "title": "Welcome to Peter\'s Hats",
                                "image_url": "http://centinela.mx/wp-content/uploads/2015/07/650_12001.jpg",
                                "subtitle": "We\'ve got the right hat for everyone.",
                                "default_action": {
                                    "type": "web_url",
                                    "url": "http://codigopanda.com/",
                                    "messenger_extensions": True,
                                    "webview_height_ratio": "tall",
                                    "fallback_url": "http://codigopanda.com/"
                                },
                                "buttons": [
                                    {
                                        "type": "web_url",
                                        "url": "http://codigopanda.com/",
                                        "title": "View Website"
                                    }, {
                                        "type": "postback",
                                        "title": "Start Chatting",
                                        "payload": "casa"
                                    }
                                ]
                            }
                        ]
                    }
                }
            }
        })
        print(data)
        r = requests.post("https://graph.facebook.com/v2.6/me/messages", params=self.GetParams(),
                          headers=self.GetHeaders(), data=data)
        if r.status_code != 200:
            return r.status_code
        return r.status_code


class Button(object):
    def __init__(self, title):
        if len(title) > 20:
            raise ValueError('Button title limit is 20 characters')
        self.title = title

    def to_json(self):
        serialised = {
            'type': self.button_type,
            'title': self.title
        }
        print("paso")
        if self.button_type == 'web_url':
            serialised['url'] = self.url
        elif self.button_type == 'postback':
            serialised['payload'] = self.payload
        elif self.button_type == 'phone_number':
            serialised['payload'] = self.payload
        return json.dumps(serialised)


class ButtonUrl(Button):
    button_type = "web_url"

    def __init__(self, title, url):
        self.url = url
        super(ButtonUrl, self).__init__(title=title)


class ButtonPostback(Button):
    button_type = "postback"

    def __init__(self, title, payload):
        self.payload = payload
        super(ButtonPostback, self).__init__(title=title)


class ButtonCall(Button):
    button_type = "phone_number"

    def __init__(self, title, number):
        self.payload = number
        super(ButtonCall, self).__init__(title=title)


class ButtonTemplate(object):
    template_type = 'button'

    def __init__(self, text, buttons):
        self.text = text

        if not isinstance(buttons, list):
            raise ValueError(
                'buttons should be a list of Button'
            )
        self.buttons = buttons

    def to_dict(self):
        return json.dumps({
            "type": "template",
            "payload": {
                'template_type': self.template_type,
                'text': self.text,
                'buttons': [
                    button.to_json() for button in self.buttons
                    ]
            }
        })


class GenericTemplate(object):
    template_type = 'generic'

    def __init__(self, text, buttons):
        self.text = text

        if not isinstance(buttons, list):
            raise ValueError(
                'buttons should be a list of Button'
            )
        self.buttons = buttons

    def to_dict(self):
        return json.dumps({
            "type": "template",
            "payload": {
                "template_type": self.template_type,
                "elements": [
                    {
                        "title": "Welcome to Peter\'s Hats",
                        "image_url": "https://i.pinimg.com/736x/42/b5/d8/42b5d8de9e1080b0acca1f82e8c0bcf2--funny-animal-jokes-funny-jokes.jpg",
                        "subtitle": "We\'ve got the right hat for everyone.",
                        "default_action": {
                            "type": "web_url",
                            "url": "http://codigopanda.com/",
                            "messenger_extensions": True,
                            "webview_height_ratio": "tall",
                            "fallback_url": "http://codigopanda.com/"
                        },
                        'buttons': [
                            button.to_json() for button in self.buttons
                            ]
                    }
                ]
            }
        })
