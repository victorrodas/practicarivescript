from rivescript import RiveScript


class MiRiveScript:
    rs = ""

    def __init__(self):
        pass

    def register(self, data):
        self.rs = RiveScript()
        self.rs.stream(data)
        self.rs.sort_replies()

    def pregunta(self, pregunta):
        return self.rs.reply("localuser", pregunta)


rive = MiRiveScript()
