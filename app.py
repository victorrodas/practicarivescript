from flask import Flask, request
from handlers.facebook import facebook_handler

app = Flask(__name__)


@app.route("/<page>/", methods=['GET', 'POST'])
def Home(page):
    if page == "facebook":
        return facebook_handler.dispatch(request=request)
    if page == "telegram":
        return "hola como estas soy telegram", 200
    return page, 200


if __name__ == '__main__':
    app.run(debug=True, port=5000)
