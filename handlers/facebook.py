from utils.rivescript import rive
from pycoremessenger import core

TOKEN_PAGE = ""
messenger = core.Core("hola", TOKEN_PAGE)


class Fbmessenger:
    def dispatch(self, request):
        print(request.method)
        if request.method == 'GET':
            if request.args.get("hub.mode") == "subscribe" and request.args.get("hub.challenge"):
                if not request.args.get("hub.verify_token") == "hola":
                    return "Verification token mismatch", 403
                return request.args["hub.challenge"], 200

            return "Hello world", 200
        if request.method == 'POST':
            data = request.get_json()
            messenger.InsertJson(data)
            if messenger.WhatEvent() == "messaging":
                if not messenger.IsMyMessage():
                    print(messenger.GetResult())
                    if messenger.GetResult():
                        rive.register(
                            ['+ hola', '- quien eres?', '+ quien es rances',
                             '- El es mi maestro el me enseño todo lo que se \s ', '+ rances murio',
                             '- no puede ser!!! me vengare de la humanidad, desearan no haber nacido', '+ me llamo *',
                             '- mucho gusto <star1> tienes alguna pregunta?'])
                    messenger.SendMessage(messenger.GetSenderId(), rive.pregunta(messenger.GetResult()))
            if messenger.WhatEvent() == "standby":
                messenger.SendMessage(messenger.GetSenderId(), "Respondiste el Payload")
            return "ok", 200


facebook_handler = Fbmessenger()
